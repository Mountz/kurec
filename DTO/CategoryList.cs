﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KUREC.DTO
{
    public class CategoryList
    {
        public List<string> Category = new List<string>();
        public CategoryList(CategoryFilter filter)
        {
            if(filter.Science)
            {
                Category.Add("Science");
            }
            if (filter.Human)
            {
                Category.Add("Human");
            }
            if (filter.Language)
            {
                Category.Add("Language");
            }
            if (filter.Social)
            {
                Category.Add("Social");
            }
        }
    }

    
}
