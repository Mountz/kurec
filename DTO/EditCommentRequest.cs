﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace KUREC.DTO
{
    public class EditCommentRequest
    {
        [Required]
        public string Code { get; set; }
        [Required]
        public int? Number { get; set; }
        [Required]
        public string Text { get; set; }
    }
}
