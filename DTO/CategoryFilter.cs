﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KUREC.DTO
{
    public class CategoryFilter
    {
        [Required]
        public bool Science { get; set; }
        [Required]
        public bool Social { get; set; }
        [Required]
        public bool Language { get; set; }
        [Required]
        public bool Human { get; set; }
    }
}
