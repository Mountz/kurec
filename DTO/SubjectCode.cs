﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KUREC.DTO
{
    public class SubjectCode
    {
        [Required]
        public string Code { get; set; }
    }
}
