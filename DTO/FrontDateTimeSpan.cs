﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KUREC.Models;
using KUREC.Helper;

namespace KUREC.DTO
{
    public class FrontDateTimeSpan
    {
        public int Day { get; set; }
        public int Start { get; set; }
        public int End { get; set; }

        public static FrontDateTimeSpan Convert(DateTimeSpanModel dt)
        {
            var converted = new FrontDateTimeSpan();
            converted.Day = DateTimeHelper.DayNameToDayOfWeek(dt.Day);
            converted.Start = DateTimeHelper.TimeStringToTimeInSecond(dt.StartTime);
            converted.End = DateTimeHelper.TimeStringToTimeInSecond(dt.EndTime);
            return converted;
        }
    }
}
