﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KUREC.DTO
{
    public class DateTimeRequestModel
    {
        [Required]
        public int Day { get; set; }
        [Required] 
        public string StartTime { get; set; }
        [Required] 
        public string EndTime { get; set; }
    }
}
