﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KUREC.Models;

namespace KUREC.DTO
{
    public class FrontPlan
    {
        public int Id { get; set; }
        public List<FrontSubject> Subjects { get; set; }

        public static FrontPlan Convert(PlanModel plan, List<SubjectModel> subjects)
        {
            var converted = new FrontPlan
            {
                Id = plan.Number,
                Subjects = new List<FrontSubject>()
            };
            Console.WriteLine(subjects.Count());
            foreach (var c in plan.Courses)
            {
                Console.WriteLine(c.SubjectCode);
                var subject = subjects.Find(s => s.Code == c.SubjectCode);
                var frontSubject = FrontSubject.Convert(subject, c.Section, c.Type);
                converted.Subjects.Add(frontSubject);
            }
            return converted;
        }
    }
}
