﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KUREC.DTO
{
    public class CourseHeader
    {
        [Required]
        public string SubjectCode { get; set; }
        [Required]
        public int Section { get; set; }
        [Required]
        public string Type { get; set; }
    }
}
