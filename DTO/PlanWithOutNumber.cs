﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace KUREC.DTO
{
    public class PlanWithOutNumber
    {
        [Required]
        public List<CourseHeader> Courses { get; set; }
    }
}
