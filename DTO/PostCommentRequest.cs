﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace KUREC.DTO
{
    public class PostCommentRequest
    {
        [Required]
        public string Code { get; set; }
        [Required]
        public string Text { get; set; }
    }
}
