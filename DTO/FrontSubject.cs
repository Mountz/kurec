﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KUREC.Models;

namespace KUREC.DTO
{
    public class FrontSubject
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Teacher { get; set; }
        public int Sec { get; set; }
        public List<FrontDateTimeSpan> SubjectInterval { get; set; }
        public string Type { get; set; }
        public int Credit { get; set; }
        public List<string> Classroom { get; set; }

        public static FrontSubject Convert(SubjectModel subject, int sec, string type)
        {
            var i = subject.Courses.FindIndex(c => c.Section == sec && c.Type == type);
            var converted = new FrontSubject
            {
                SubjectInterval = new List<FrontDateTimeSpan>(),
                Classroom = new List<string>(),
                Id = subject.Code,
                Name = subject.Name,
                Teacher = subject.Courses[i].Teacher,
                Sec = subject.Courses[i].Section,
                Type = subject.Courses[i].Type,
                Credit = subject.Courses[i].Credit
            };
            foreach (var classroom in subject.Courses[i].Classroom)
            {
                converted.Classroom.Add(classroom);
            }
            foreach (var time in subject.Courses[i].DateTime)
            {
                converted.SubjectInterval.Add(FrontDateTimeSpan.Convert(time));
            }
            return converted;
        }
    }
}
