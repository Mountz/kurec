﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KUREC.DTO
{
    public class SubjectWithCat
    {
        public string Code { get; set; }
        public string Category { get; set; }
    }
}
