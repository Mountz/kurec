﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KUREC.Helper
{
    public class DateTimeHelper
    {
        public static int TimeStringToTimeInSecond(string timeString)
        {
            if (!timeString.Contains(":"))
            {
                return -1;
            }
            string[] splited = timeString.Split(':');
            int hour = Int32.Parse(splited[0]);
            int minute = Int32.Parse(splited[1]);
            return hour * 3600 + minute * 60;
        }

        public static string TimeInSecondToTimeString(int time)
        {
            if (time < 0)
            {
                return "ไม่มีข้อมูล";
            }
            int hour = time / 3600;
            int min = (time - (hour * 3600)) / 60;
            return hour.ToString() + ":" + min.ToString();
        }

        private static readonly string[] dayNameArray = { "Sunday", "Monday", 
                                                     "Tuesday", "Wednesday",
                                                     "Thursday", "Friday", 
                                                     "Saturday" };

        public static string DayOfWeekToDayName(int dow)
        {
            if (dow >= dayNameArray.Length)
            {
                return "ไม่มีข้อมูล";
            }
            return dayNameArray[dow];
        }

        public static int DayNameToDayOfWeek(string dayName)
        {
            return Array.IndexOf(dayNameArray, dayName);
        }
    }
}
