﻿FROM mcr.microsoft.com/dotnet/core/aspnet:3.0-buster-slim AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/core/sdk:3.0-buster AS builder
# Resolve dependencies
WORKDIR /src
COPY "KUREC.csproj" .
RUN dotnet restore "KUREC.csproj"

COPY . .
RUN dotnet build "KUREC.csproj" -c Release -o /app/build

FROM builder AS publisher
RUN dotnet publish "KUREC.csproj" -c Release -o /app/publish

FROM base AS runtime
WORKDIR /app
COPY --from=publisher /app/publish .

EXPOSE 5000/tcp
ENV ASPNETCORE_URLS=http://+:5000
ENTRYPOINT ["dotnet", "KUREC.dll"]