﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using KUREC.Services;
using KUREC.DTO;

namespace KUREC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TopicsController : ControllerBase
    {
        private readonly ITopicService _topicService;
        // GET: api/Topic
        public TopicsController(ITopicService topicService)
        {
            _topicService = topicService;
        }

        // GET: api/Topic
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _topicService.GetAll();
            return Ok(new { result });
        }

        // GET: api/Topic/5
        [HttpGet("{code}")]
        public async Task<IActionResult> Get(string code)
        {
            var result = await _topicService.Get(code);
            return Ok(new { result });
        }

        [HttpGet("searchbykeyword")]
        public async Task<IActionResult> SearchByKeyword([FromQuery(Name = "q")] string query)
        {
            var result = await _topicService.SearchByKeyword(query);
            return Ok(new { result });
        }

        // POST: api/Topic/comment
        [HttpPost("comment/")]
        [Authorize]
        public async Task<IActionResult> Post([FromBody] PostCommentRequest body)
        {
            var email = User.Identity.Name;
            await _topicService.PostComment(body.Code, body.Text, email);
            return Ok();
        }

        [HttpPost("comment/like")]
        [Authorize]
        public async Task<IActionResult> Like([FromBody] TopicCommentCode body)
        {
            var email = User.Identity.Name;
            await _topicService.LikeComment(body.Code, body.Number.Value, email);
            return Ok();
        }

        [HttpPost("comment/dislike")]
        [Authorize]
        public async Task<IActionResult> Dislike([FromBody] TopicCommentCode body)
        {
            var email = User.Identity.Name;
            await _topicService.DislikeComment(body.Code, body.Number.Value, email);
            return Ok();
        }

        // PUT: api/Topic/5
        [HttpPut("comment/")]
        [Authorize]
        public async Task<IActionResult> EditComment([FromBody] EditCommentRequest body)
        {
            var email = User.Identity.Name;
            await _topicService.EditComment(body.Code, body.Number.Value, body.Text, email);
            return Ok();
        }

        // DELETE: api/topics/comment
        [HttpDelete("comment")]
        public async Task<IActionResult> RemoveComment([FromBody] TopicCommentCode body)
        {
            var email = User.Identity.Name;
            var result = await _topicService.RemoveComment(body.Code, body.Number.Value, email);
            if (result)
            {
                return Ok();
            } else
            {
                return Unauthorized();
            }
        }

    }
}
