﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using KUREC.Models;
using KUREC.Services;

namespace KUREC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        private readonly ITodoService todoService;
        public TodoController(ITodoService todoService)
        {
            this.todoService = todoService;
        }

        [HttpPost("")]
        public async Task<IActionResult> AddTodo([FromBody] TodoModel model)
        {
            bool result = await todoService.AddAsync(model);
            Console.WriteLine(result);
            if (result)
            {
                return Ok();
            }
            else
            {
                return StatusCode(500);
            }
        }

        [HttpGet("")]
        public async Task<IActionResult> GetTodo()
        {
            TodoModel[] result = await todoService.GetAllAsync();
            return Ok(result);
        }

    }
}