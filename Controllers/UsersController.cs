﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using KUREC.Models;
using KUREC.DTO;
using KUREC.Services;
using System.IO;

namespace KUREC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {

        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }
        // GET: api/Users
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var cur = HttpContext.User.Identity.Name;
            Console.WriteLine(cur);
            var users = await _userService.GetAllAsync();
            return Ok(users);
        }

        // GET: api/Users/5
        [HttpGet("{email}")]
        
        public async Task<IActionResult> Get(string email)
        {
            var result = await _userService.GetOneUser(email);
            if (result != null)
            {
                return Ok(result);
            }
            return StatusCode(404);
        }

        // POST: api/Users/register
        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody] RegisterModel user)
        {
            var result = await _userService.RegisterAsync(UserModel.fromRegisterModel(user));
            if (result != null)
            {
                return Ok(result);
            }
            if (user.Password != user.ConfirmPassword)
            {
                return StatusCode(400);
            }
            return StatusCode(500);
        }

        // POST: api/Users/login
        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] LoginModel user)
        {
            // Console.WriteLine("{0} {1}", user.email, user.password);
            var result = await _userService.LoginAsync(user.email, user.password);
            //return Ok(user);
            Console.WriteLine(result);
            if (result != null)
            {
                return Ok(result);
            }
            return Unauthorized();
        }


        // PUT: api/Users/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        [HttpPost("favorite")]
        public async Task<IActionResult> AddFavorite([FromBody] SubjectCode body)
        {
            var email = User.Identity.Name;
            await _userService.AddFavorite(email, body.Code);
            return Ok();
        }

        [HttpDelete("favorite")]
        public async Task<IActionResult> UnFavorite([FromBody] SubjectCode body)
        {
            var email = User.Identity.Name;
            await _userService.UnFavorite(email, body.Code);
            return Ok();
        }
    }
}
