﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using KUREC.Services;
using KUREC.DTO;
using System.IO;

namespace KUREC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PlansController : ControllerBase
    {
        // GET: api/Plans
        private readonly IPlanService _planService;

        public PlansController(IPlanService planService)
        {
            _planService = planService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllPlan()
        {
            var email = User.Identity.Name;
            var result = await _planService.GetAllAsync(email);
            return Ok(new { result });
        }

        // GET: api/Plans/5
        [HttpGet("{number}")]
        public async Task<IActionResult> GetPlanByNumber(int number)
        {
            var email = User.Identity.Name;
            var result = await _planService.GetAsyncByNumber(email, number);
            return Ok(new { result });
        }

        // POST: api/Plans
        [HttpPost]
        public async Task<IActionResult> AddNewPlan([FromBody] PlanWithOutNumber plan)
        {
            var email = User.Identity.Name;
            await _planService.CreateAsync(email, plan);
            return Ok();
        }

        // PUT: api/Plans/5
        [HttpPut("{number}")]
        public async Task<IActionResult> Put(int number, [FromBody] PlanWithOutNumber plan)
        {
            var email = User.Identity.Name;
            await _planService.UpdatePlan(email, number, plan);
            return Ok();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{number}")]
        public async Task<IActionResult> Delete(int number)
        {
            var email = User.Identity.Name;
            await _planService.RemovePlan(email, number);
            return Ok();
        }
    }
}
