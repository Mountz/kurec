﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using KUREC.Services;
using KUREC.DTO;

namespace KUREC.Models
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubjectsController : ControllerBase
    {
        private readonly ISubjectService _subjectService;

        public SubjectsController(ISubjectService subjectService)
        {
            _subjectService = subjectService;
        }

        // GET: api/Subjects
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            IEnumerable<SubjectModel> result = await _subjectService.GetAllAsync();
            return Ok(new { result });
        }

        // GET: api/Subjects/F
        [HttpGet("FindByKeyword")]
        public async Task<IActionResult> FindByKeyword([FromQuery(Name ="q")] string query)
        {
            List<SubjectModel> result = await _subjectService.FindByKeyword(query);
            return Ok(new { result }); ;
        }

        // GET: api/Subjects/5
        [HttpGet("FindByCode")]
        public async Task<IActionResult> FindByCode([FromQuery(Name = "q")] string query)
        {
            List<SubjectModel> result = await _subjectService.FindByCode(query);
            return Ok(new { result });
        }

        [HttpPost("FindByCategory")]
        public async Task<IActionResult> FindByCategory([FromBody] CategoryFilter filter)
        {
            List<SubjectModel> result = await _subjectService.FindByCategory(filter);
            return Ok(new { result });
        }

        // GET: api/Subjects/findByDateTime
        [HttpPost("findByDateTime")]
        public async Task<IActionResult> GetByDateTime([FromBody] List<DateTimeRequestModel> body)
        {
            List<DateTimeSpanModel> dateTimeList = new List<DateTimeSpanModel>();
            foreach (var dt in body)
            {
                dateTimeList.Add(DateTimeSpanModel.FromRequestModel(dt));
            }
            List<SubjectModel> result = await _subjectService.GetByDateTime(dateTimeList);
            return Ok(result);
        }
    }
}
