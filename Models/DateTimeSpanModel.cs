﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using KUREC.Helper;
using KUREC.DTO;

namespace KUREC.Models
{
    public class DateTimeSpanModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }
        [BsonElement("day")] public string Day { get; set; }
        [BsonElement("startTime")] public string StartTime { get; set; }
        [BsonElement("endTime")] public string EndTime { get; set; }

        public static DateTimeSpanModel FromRequestModel(DateTimeRequestModel dt)
        {
            DateTimeSpanModel newDateTime = new DateTimeSpanModel
            {
                Day = DateTimeHelper.DayOfWeekToDayName(dt.Day),
                StartTime = dt.StartTime,
                EndTime = dt.EndTime
            };
            return newDateTime;
        }

    }
}
