﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace KUREC.Models
{
    public class PlanModel
    {
        public PlanModel()
        {
            Courses = new List<CourseHeaderModel>();
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }
        [BsonElement("number")] public int Number { get; set; }
        [BsonElement("courses")] public List<CourseHeaderModel> Courses { get; set; }
    }
}
