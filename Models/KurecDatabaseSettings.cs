﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KUREC.Models
{
    public class KurecDatabaseSettings : IKurecDatabaseSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
        public string SubjectsCollectionName { get; set; }
        public string UsersCollectionName { get; set; }
        public string SubjectTopicsCollectionName { get; set; }
    }

    public interface IKurecDatabaseSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
        public string SubjectsCollectionName { get; set; }
        public string UsersCollectionName { get; set; }
        public string SubjectTopicsCollectionName { get; set; }
    }
}
