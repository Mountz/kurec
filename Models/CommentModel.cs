﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace KUREC.Models
{
    public class CommentModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }
        [BsonElement("number")] public int Number { get; set; }
        [BsonElement("user")] public string User { get; set; }
        [BsonElement("text")] public string Text { get; set; }
        [BsonElement("like")] public List<string> LikeUser { get; set; }
        [BsonElement("dislike")] public List<string> DislikeUser { get; set; }
        [BsonElement("datetime")] public long DateTime { get; set; }
    }
}
