﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KUREC.DTO;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace KUREC.Models
{
    public class UserModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }
        [BsonElement("firstname")] public string FirstName { get; set; }
        [BsonElement("lastname")] public string LastName { get; set; }
        [BsonElement("code")]public string Code { get; set; }
        [BsonElement("email")] public string Email { get; set; }
        [BsonElement("password")] public string Password { get; set; }
        [BsonElement("major")] public string Major { get; set; }
        [BsonElement("plans")] public List<PlanModel> Plans { get; set; }
        [BsonElement("token")] public string Token { get; set; }
        [BsonElement("favorite")] public List<string> FavoriteSubjectCode { get; set; }


        public static UserModel fromRegisterModel(RegisterModel model)
        {
            UserModel newUser = new UserModel();
            newUser.FirstName = model.FirstName;
            newUser.LastName = model.LastName;
            newUser.Code = model.Code;
            newUser.Email = model.Email;
            newUser.Password = model.Password;
            newUser.Major = model.Major;
            newUser.Plans = new List<PlanModel>();
            newUser.FavoriteSubjectCode = new List<string>();
            return newUser;
        }
    }
}
