﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace KUREC.Models
{
    [BsonIgnoreExtraElements]
    public class SubjectTopicModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }

        [BsonElement("code")] public string Code { get; set; }
        [BsonElement("name")] public string Name { get; set; }
        [BsonElement("description")] public string Description { get; set; }
        [BsonElement("category")] public string[] Category { get; set; }
        [BsonElement("comments")] public List<CommentModel> Comments { get; set; }
    }
}
