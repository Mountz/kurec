﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using KUREC.DTO;

namespace KUREC.Models
{
    public class CourseHeaderModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }
        [BsonElement("subjectCode")] public string SubjectCode { get; set; }
        [BsonElement("section")] public int Section { get; set; }
        [BsonElement("type")] public string Type { get; set; }

        public static CourseHeaderModel From(CourseHeader header)
        {
            CourseHeaderModel newHeader = new CourseHeaderModel
            {
                Section = header.Section,
                SubjectCode = header.SubjectCode,
                Type = header.Type
            };
            return newHeader;
        }
    }
}
