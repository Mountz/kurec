﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace KUREC.Models
{
    public class DateTimeSpanMongoModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }
        [BsonElement("day")] public string Day { get; set; }
        [BsonElement("startTime")] public string StartTime { get; set; }
        [BsonElement("endTime")] public string EndTime { get; set; }

    }
}
