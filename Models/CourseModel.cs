﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using KUREC.Models;

namespace KUREC.Models
{
    public class CourseModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }

        [BsonElement("type")] public string Type { get; set; }

        [BsonElement("credit")] public int Credit { get; set; }
        
        [BsonElement("section")] public int Section { get; set; }   
        [BsonElement("dateTime")] public List<DateTimeSpanModel> DateTime { get; set; }
        [BsonElement("classroom")] public string[] Classroom { get; set; }  
        [BsonElement("teacher")] public string Teacher { get; set; }
        [BsonElement("maxStudent")] public int MaxStudent { get; set; }
        [BsonElement("curStudent")] public int CurStudent { get; set; }
        [BsonElement("majorConstrain")] public string MajorConstrain { get; set; }
    }
}
