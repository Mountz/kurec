﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace KUREC.Models
{
    [BsonIgnoreExtraElements]
    public class SubjectModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }

        [BsonElement("code")]
        public string Code { get; set; }

        [BsonElement("name")] public string Name { get; set; }

        [BsonElement("category")] public string[] Category { get; set; }

        [BsonElement("courses")] public List<CourseModel> Courses { get; set; }
    }
}
