﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KUREC.Models;
using KUREC.DTO;

namespace KUREC.DAL
{
    interface ISubjectDal
    {
        public Task<List<SubjectModel>> GetAllAsync();

        public Task<SubjectModel> GetAsync(string code);

        public Task<List<SubjectModel>> GetByDateTime(DateTimeSpanModel dateTimeList);

        public Task<SubjectModel> CreateAsync(SubjectModel subject);

        public Task<List<SubjectModel>> FindByKeyword(string query);

        public Task<List<SubjectModel>> FindByCode(string query);
        public Task<List<SubjectModel>> FindByCategory(CategoryList categoryList);
        public void UpdateAsync(string code, SubjectModel pSubject);

        public void RemoveAsync(string code);
        
    }
}
