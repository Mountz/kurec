﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using KUREC.Helper;
using KUREC.Models;
using KUREC.DTO;

namespace KUREC.DAL
{
    public class SubjectDal : ISubjectDal
    {
        private readonly IMongoCollection<SubjectModel> _subjects;

        public SubjectDal(IKurecDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _subjects = database.GetCollection<SubjectModel>(settings.SubjectsCollectionName);
        }

        public async Task<List<SubjectModel>> GetAllAsync()
        {
            List<SubjectModel> result = new List<SubjectModel>();
            IAsyncCursor<SubjectModel> cursor = await _subjects.FindAsync(subject => true);
            result = await cursor.ToListAsync();
            return result;
        }

        public async Task<SubjectModel> GetAsync(string code)
        {
            
            IAsyncCursor<SubjectModel> list = await _subjects.FindAsync(subject => subject.Code == code);
            SubjectModel result = await list.FirstOrDefaultAsync();
            return result;
        }

        public async Task<SubjectModel> CreateAsync(SubjectModel subject)
        {
            await _subjects.InsertOneAsync(subject);
            return subject;
        }

        public void UpdateAsync(string code, SubjectModel pSubject)
        {
            //throw new Exception(new NotImplementedException());
            //_subjects.ReplaceOne(subject => subject.Code == code, pSubject);
        }

        public void RemoveAsync(string code)
        {
            _subjects.DeleteOne(subject => subject.Code == code);
        }

        public async Task<List<SubjectModel>> GetByDateTime(DateTimeSpanModel dateTime)
        {
            List<SubjectModel> result = new List<SubjectModel>();
            Console.WriteLine("{0} === {1} === {2}", dateTime.Day, dateTime.StartTime, dateTime.EndTime);
            IAsyncCursor<SubjectModel> cursor = await _subjects.FindAsync(s => s.Courses.Any(
                c => c.DateTime.Any(
                    dt => dt.Day.Equals(dateTime.Day) &&
                    dt.StartTime == dateTime.StartTime &&
                    dt.EndTime == dateTime.EndTime
                )
            ));
            
            return result;
        }

        public async Task<List<SubjectModel>> FindByKeyword(string query)
        {
            string regex = ".*" + query + ".*";
            var filter = Builders<SubjectModel>.Filter.Regex("name", new BsonRegularExpression(regex, "i"));
            var result = await _subjects.FindAsync(filter);
            return await result.ToListAsync();
        }

        public async Task<List<SubjectModel>> FindByCode(string query)
        {
            string regex = ".*" + query + ".*";
            var filter = Builders<SubjectModel>.Filter
                            .Regex("code", regex);
            var result = await _subjects.FindAsync(filter);
            return await result.ToListAsync();
        }

        public async Task<List<SubjectModel>> FindByCategory(CategoryList categoryList)
        {
            var list = new List<SubjectModel>();
            foreach (var filter in categoryList.Category)
            {
                var cursor = await _subjects.FindAsync(s => s.Category.Any(category => category == filter));
                var result = await cursor.ToListAsync();
                list.AddRange(result);
            }
            return list;
        }
    }
}
