﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KUREC.Models;

namespace KUREC.DAL
{
    interface ITopicDal
    {
        public Task<SubjectTopicModel> Get(string code);
        public Task<List<SubjectTopicModel>> GetAll();
        public Task<List<SubjectTopicModel>> SearchByKeyword(string q);
        public Task PostComment(string code, CommentModel comment);
        public Task RemoveComment(string code, int number);
        public Task EditComment(string code, int number, string email);
        public Task LikeComment(string code, int number, string email);
        public Task DislikeComment(string code, int number, string email);
        public Task RemoveLikeComment(string code, int number, string email);
        public Task RemoveDislikeComment(string code, int number, string email);
    }
}
