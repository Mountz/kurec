﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using KUREC.Models;
using KUREC.Helper;
using KUREC.DTO;

namespace KUREC.DAL
{
    public class UserDal : IUserDal
    {
        private readonly IMongoCollection<UserModel> _users;
        public UserDal(IKurecDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _users = database.GetCollection<UserModel>(settings.UsersCollectionName);
        }

        public async Task<UserModel> Authenticate(string email, string password)
        {
            var cursor = await _users.FindAsync(u => u.Email == email && u.Password == password);
            var list = cursor.ToList();

            if (list.Count == 0)
            {
                Console.WriteLine("User Not Found");
                return null;
            }
            var user = list.First();
            Console.WriteLine("{0}", user.FirstName);
            return user.WithoutPassword();
        }

        public async Task<UserModel> CreateAsync(UserModel newUser)
        {
            await _users.InsertOneAsync(newUser);
            var cursor = await _users.FindAsync(user => user.Code == newUser.Code);
            var list = cursor.ToList();
            if (list.Count == 0)
            {
                Console.WriteLine("Registration Unsuccess");
                return null;
            }
            Console.WriteLine("Registration Success");
            return list.First().WithoutPassword();
        }

        public async Task CreatePlan(string email, PlanModel newPlan)
        {
            var filter = Builders<UserModel>.Filter.Eq(u => u.Email, email);
            var update = Builders<UserModel>.Update.Push("plans", newPlan);
            var result = await _users.UpdateOneAsync(filter, update);
            return ;
        }

        public async Task<List<UserModel>> GetAllAsync()
        {
            var cursor = await _users.FindAsync(u => true);
            var list = await cursor.ToListAsync();
            list = list.Select(u => u.WithoutPassword()).ToList();
            return list;
        }

        public async Task<List<PlanModel>> GetAllPlan(string email)
        {
            var cursor = await _users.FindAsync(u => u.Email == email);
            var list = await cursor.ToListAsync();
            if (list.Count == 0)
            {
                return null;
            }
            var user = list.First();
            return user.Plans;
        }

        public async Task<UserModel> GetByEmailAsync(string email)
        {
            var cursor = await _users.FindAsync(u => u.Email == email);
            var list = cursor.ToList();
            if (list.Count == 0)
            {
                Console.WriteLine("User Not Found");
                return null;
            }
            var user = list.First();
            return user.WithoutPassword();
        }

        public async Task<PlanModel> GetPlanByNumber(string email, int planNumber)
        {
            var cursor = await _users.FindAsync(u => u.Email == email);
            var list = await cursor.ToListAsync();
            if (list.Count == 0)
            {
                return null;
            }
            var user = list.First();
            var plan = user.Plans.Find(p => p.Number == planNumber);
            return plan;
        }

        public void RemoveAsync(string code)
        {
            throw new NotImplementedException();
        }

        public async Task RemovePlan(string email, int planNumber)
        {
            var filter = Builders<UserModel>.Filter.Eq(u => u.Email, email);
            var update = Builders<UserModel>.Update.PullFilter("plans", Builders<UserModel>.Filter.Eq("number", planNumber));

            var result = await _users.UpdateOneAsync(filter, update);
            return;
        }

        public void UpdateAsync(string code, UserModel pUser)
        {
            throw new NotImplementedException();
        }

        public async Task UpdatePlan(string email, int planNumber, PlanModel newPlan)
        {
            var filter = Builders<UserModel>.Filter.Eq(u => u.Email, email)
                & Builders<UserModel>.Filter.Eq("plans.number", planNumber);
            var update = Builders<UserModel>.Update.Set("plans.$", newPlan);
            var result = await _users.UpdateOneAsync(filter, update);
            return;
        }

        public async Task AddFavorite(string email, string code)
        {
            var filter = Builders<UserModel>.Filter.Eq("email", email);
            var update = Builders<UserModel>.Update.Push("favorite", code);
            await _users.UpdateOneAsync(filter, update);
            return;
        }

        public async Task UnFavorite(string email, string code)
        {
            var filter = Builders<UserModel>.Filter.Eq("email", email);
            var update = Builders<UserModel>.Update.Pull("favorite", code);
            await _users.UpdateOneAsync(filter, update);
            return;
        }
    }
}
