﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KUREC.Models;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using MongoDB.Bson;

namespace KUREC.DAL
{
    public class TopicDal : ITopicDal
    {
        private readonly IMongoCollection<SubjectTopicModel> _topics;

        public TopicDal(IKurecDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _topics = database.GetCollection<SubjectTopicModel>(settings.SubjectTopicsCollectionName);
        }

        public async Task EditComment(string code, int number, string newText)
        {
            var filter = Builders<SubjectTopicModel>.Filter.And(
                    Builders<SubjectTopicModel>.Filter.Where(t => t.Code == code),
                    Builders<SubjectTopicModel>.Filter.Eq("comments.number", number));
            var update = Builders<SubjectTopicModel>.Update.Set("comments.$.text", newText);
            var result = await _topics.FindOneAndUpdateAsync(filter, update);
            return;
        }

        public async Task<SubjectTopicModel> Get(string code)
        {
            var list = await _topics.FindAsync(subject => subject.Code == code);
            var result = await list.FirstOrDefaultAsync();
            return result;
        }

        public async Task<List<SubjectTopicModel>> GetAll()
        {
            var result = new List<SubjectTopicModel>();
            var cursor = await _topics.FindAsync(t => true);
            result = await cursor.ToListAsync();
            return result;
        }

        public async Task LikeComment(string code, int number, string email)
        {
            var filter = Builders<SubjectTopicModel>.Filter.And(
                    Builders<SubjectTopicModel>.Filter.Where(t => t.Code == code),
                    Builders<SubjectTopicModel>.Filter.Eq("comments.number", number));
            var update = Builders<SubjectTopicModel>.Update.Push("comments.$.like", email);
            var result = await _topics.FindOneAndUpdateAsync(filter, update);
            return;
        }

        public async Task DislikeComment(string code, int number, string email)
        {
            var filter = Builders<SubjectTopicModel>.Filter.And(
                    Builders<SubjectTopicModel>.Filter.Where(t => t.Code == code),
                    Builders<SubjectTopicModel>.Filter.Eq("comments.number", number));
            var update = Builders<SubjectTopicModel>.Update.Push("comments.$.dislike", email);
            var result = await _topics.FindOneAndUpdateAsync(filter, update);
            return;
        }

        public async Task PostComment(string code, CommentModel newComment)
        {
            var filter = Builders<SubjectTopicModel>.Filter.Eq(t => t.Code, code);
            var update = Builders<SubjectTopicModel>.Update.Push("comments", newComment);
            var result = await _topics.UpdateOneAsync(filter, update);
            return;
        }

        public async Task RemoveComment(string code, int number)
        {
            Console.WriteLine("{0}:{1}", code, number);
            var filter = Builders<SubjectTopicModel>.Filter.Eq(t => t.Code, code);
            var update = Builders<SubjectTopicModel>.Update.PullFilter( s => s.Comments,
                c => c.Number == number);
            var result = await _topics.FindOneAndUpdateAsync(filter, update);
            return;
        }

        public async Task RemoveLikeComment(string code, int number, string email)
        {
            var filter = Builders<SubjectTopicModel>.Filter.And(
                    Builders<SubjectTopicModel>.Filter.Where(t => t.Code == code),
                    Builders<SubjectTopicModel>.Filter.Eq("comments.number", number));
            var update = Builders<SubjectTopicModel>.Update.Pull("comments.$.like", email);
            var result = await _topics.FindOneAndUpdateAsync(filter, update);
            return;
        }

        public async Task RemoveDislikeComment(string code, int number, string email)
        {
            var filter = Builders<SubjectTopicModel>.Filter.And(
                    Builders<SubjectTopicModel>.Filter.Where(t => t.Code == code),
                    Builders<SubjectTopicModel>.Filter.Eq("comments.number", number));
            var update = Builders<SubjectTopicModel>.Update.Pull("comments.$.dislike", email);
            var result = await _topics.FindOneAndUpdateAsync(filter, update);
            return;
        }

        public async Task<List<SubjectTopicModel>> SearchByKeyword(string q)
        {
            string regex = ".*" + q + ".*";
            var filter = Builders<SubjectTopicModel>.Filter.Or(
                Builders<SubjectTopicModel>.Filter.Regex("name", new BsonRegularExpression(regex, "i")),
                Builders<SubjectTopicModel>.Filter.Regex("description", new BsonRegularExpression(regex, "i")),
                Builders<SubjectTopicModel>.Filter.Regex("code", new BsonRegularExpression(regex, "i"))
            );
            var result = await _topics.FindAsync(filter);
            return await result.ToListAsync();
        }
    }
}
