﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KUREC.Models;
using KUREC.DTO;

namespace KUREC.DAL
{
    interface IUserDal
    {
        public Task<List<UserModel>> GetAllAsync();

        public Task<UserModel> GetByEmailAsync(string email);

        public Task<UserModel> CreateAsync(UserModel newUser);

        public void UpdateAsync(string code, UserModel pUser);

        public void RemoveAsync(string code);

        public Task<UserModel> Authenticate(string email, string password);

        public Task CreatePlan(string email, PlanModel newPlan);
        public Task<PlanModel> GetPlanByNumber(string email, int planNumber);
        public Task RemovePlan(string email, int planNumber);
        public Task<List<PlanModel>> GetAllPlan(string email);
        public Task UpdatePlan(string email, int planNumber, PlanModel newPlan);

        public Task AddFavorite(string email, string code);
        public Task UnFavorite(string email, string code);
    }
}
