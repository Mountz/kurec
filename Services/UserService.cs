﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Options;
using KUREC.Models;
using KUREC.Helper;
using KUREC.DAL;

namespace KUREC.Services
{
    public class UserService : IUserService
    {
        private readonly IUserDal _userDal;
        private readonly AppSettings _appSettings;

        public UserService(UserDal userDal, IOptions<AppSettings> appSettings)
        {
            _userDal = userDal;
            _appSettings = appSettings.Value;
        }

        public async Task<UserModel> LoginAsync(string email, string password)
        {
            var user = await _userDal.Authenticate(email, password);
            
            if (user == null)
            {
                return null;
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Email.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            return user;
        }

        public Task<bool> LogoutAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<UserModel> RegisterAsync(UserModel newUser)
        {
            var oldUser = await _userDal.GetByEmailAsync(newUser.Email);
            if (oldUser != null)
            {
                return null;
            }

            return await _userDal.CreateAsync(newUser);
        }

        public async Task<IEnumerable<UserModel>> GetAllAsync()
        {
            return await _userDal.GetAllAsync();
        }

        public Task<UserModel> GetOneUser(string email)
        {
            var result = _userDal.GetByEmailAsync(email);
            return result;
        }

        public async Task AddFavorite(string email, string code)
        {
            await _userDal.AddFavorite(email, code);
            return;
        }

        public async Task UnFavorite(string email, string code)
        {
            await _userDal.UnFavorite(email, code);
            return;
        }
    }
}
