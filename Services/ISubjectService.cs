﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KUREC.Models;
using KUREC.DTO;

namespace KUREC.Services
{
    public interface ISubjectService
    {
        Task<SubjectModel> CreateAsync(SubjectModel model);
        Task<List<SubjectModel>> GetAllAsync();
        Task<List<SubjectModel>> GetByDateTime(List<DateTimeSpanModel> dateTime);
        Task<SubjectModel> GetAsync(string code);
        Task<List<SubjectModel>> FindByKeyword(string query);
        Task<List<SubjectModel>> FindByCode(string query);
        Task<List<SubjectModel>> FindByCategory(CategoryFilter filter);
    }
    
}
