﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KUREC.Models;
using KUREC.DAL;

namespace KUREC.Services
{
    public class TopicService : ITopicService
    {
        private readonly ITopicDal _topicDal;

        public TopicService(TopicDal topicDal)
        {
            _topicDal = topicDal;
        }

        public async Task PostComment(string code, string text, string email)
        {
            var topic = await _topicDal.Get(code);
            var nextCommentNumber = 0;
            if (topic.Comments.Count != 0)
            {
                nextCommentNumber = topic.Comments.Last().Number + 1;
            }
            var comment = topic.Comments.Find(c => c.User == email);
            if (comment != null)
            {
                return;
            }
            
            var newComment = new CommentModel()
            {
                DateTime = Convert.ToInt64((DateTime.UtcNow - new DateTime(0)).TotalSeconds),
                User = email,
                Text = text,
                LikeUser = new List<string>(),
                DislikeUser = new List<string>(),
                Number = nextCommentNumber
            };
            await _topicDal.PostComment(code, newComment);
            return;
        }

        public async Task EditComment(string code, int number, string text, string email)
        {
            var topic = await _topicDal.Get(code);
            var comment = topic.Comments.Find(c => c.Number == number);
            if (comment.User == email)
            {
                await _topicDal.EditComment(code, number, text);
            }

            return;
        }

        public async Task<SubjectTopicModel> Get(string code)
        {
            var result = await _topicDal.Get(code);
            return result;
        }

        public async Task<List<SubjectTopicModel>> GetAll()
        {
            var result = await _topicDal.GetAll();
            return result;
        }

        public async Task LikeComment(string code, int number, string email)
        {
            var topic = await _topicDal.Get(code);
            var comment = topic.Comments.Find(c => c.Number == number);
            if (comment.LikeUser.Contains(email))
            {
                await _topicDal.RemoveLikeComment(code, number, email);
            } else
            {
                await _topicDal.LikeComment(code, number, email);
            }
            if (comment.DislikeUser.Contains(email))
            {
                await _topicDal.RemoveDislikeComment(code, number, email);
            }
        }

        public async Task DislikeComment(string code, int number, string email)
        {
            var topic = await _topicDal.Get(code);
            var comment = topic.Comments.Find(c => c.Number == number);
            if (comment.DislikeUser.Contains(email))
            {
                await _topicDal.RemoveDislikeComment(code, number, email);
            }
            else
            {
                await _topicDal.DislikeComment(code, number, email);
            }
            if (comment.LikeUser.Contains(email))
            {
                await _topicDal.RemoveLikeComment(code, number, email);
            }
            return;
        }

        

        public async Task<bool> RemoveComment(string code, int number, string email)
        {
            var topic = await _topicDal.Get(code);
            var comment = topic.Comments.Find(c => c.Number == number);
            if (comment.User == email)
            {
                await _topicDal.RemoveComment(code, number);
                return true;
            }
            return false;
        }

        public async Task<List<SubjectTopicModel>> SearchByKeyword(string q)
        {
            var result = await _topicDal.SearchByKeyword(q);
            return result;
        }
    }
}
