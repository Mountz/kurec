﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KUREC.Models;
using KUREC.DTO;

namespace KUREC.Services
{
    public interface IPlanService
    {
        Task<int> CreateAsync(string email, PlanWithOutNumber model);
        Task<List<FrontPlan>> GetAllAsync(string email);
        Task<FrontPlan> GetAsyncByNumber(string email, int number);
        Task RemovePlan(string email, int number);
        Task UpdatePlan(string email, int number, PlanWithOutNumber newPlan);
    }
}
