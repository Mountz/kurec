﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KUREC.Models;

namespace KUREC.Services
{
    public interface IUserService
    {
        public Task<UserModel> RegisterAsync(UserModel newUser);
        public Task<UserModel> LoginAsync(string email, string password);
        public Task<bool> LogoutAsync();
        public Task<IEnumerable<UserModel>> GetAllAsync();
        public Task<UserModel> GetOneUser(string email);
        public Task AddFavorite(string email, string code);
        public Task UnFavorite(string email, string code);
    }
}
