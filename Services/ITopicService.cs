﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KUREC.Models;

namespace KUREC.Services
{
    public interface ITopicService
    {
        public Task<SubjectTopicModel> Get(string code);
        public Task<List<SubjectTopicModel>> GetAll();
        public Task<List<SubjectTopicModel>> SearchByKeyword(string q);
        public Task PostComment(string code, string text, string email);
        public Task<bool> RemoveComment(string code, int number, string email);
        public Task EditComment(string code, int number, string text, string email);
        public Task LikeComment(string code, int number, string email);
        public Task DislikeComment(string code, int number, string email);
    }
}
