﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KUREC.Models;
using KUREC.DAL;
using KUREC.DTO;

namespace KUREC.Services
{
    public class PlanService : IPlanService
    {
        private readonly IUserDal _userDal;
        private readonly ISubjectDal _subjectDal;

        public PlanService(UserDal userDal, SubjectDal subjectDal)
        {
            _userDal = userDal;
            _subjectDal = subjectDal;
        }

        public async Task<int> CreateAsync(string email, PlanWithOutNumber plan)
        {
            var user = await _userDal.GetByEmailAsync(email);
            int nextPlanNumber = 0;
            if (user.Plans.Count != 0)
            {
                nextPlanNumber = user.Plans.Last().Number + 1;
            }
            var newPlan = new PlanModel();
            Console.WriteLine(plan.Courses.Count());
            foreach (var c in plan.Courses)
            {
                Console.WriteLine("{0} {1}",c.SubjectCode, c.Section);
                newPlan.Courses.Add(CourseHeaderModel.From(c));
            }
            newPlan.Number = nextPlanNumber;
            await _userDal.CreatePlan(email, newPlan);
            var plans = await _userDal.GetAllPlan(email);
            return plans.Last().Number;
        }

        public async Task<List<FrontPlan>> GetAllAsync(string email)
        {
            var plans = await _userDal.GetAllPlan(email);
            var subjectList = new List<SubjectModel>();
            var result = new List<FrontPlan>();
            foreach (var p in plans)
            {
                foreach (var c in p.Courses)
                {
                    var subject = await _subjectDal.GetAsync(c.SubjectCode);
                    subjectList.Add(subject);
                }
                var frontPlan = FrontPlan.Convert(p, subjectList);
                result.Add(frontPlan);
            }
            return result;
        }

        public async Task<FrontPlan> GetAsyncByNumber(string email, int number)
        {
            var plan = await _userDal.GetPlanByNumber(email, number);
            var subjectList = new List<SubjectModel>();
            foreach (var c in plan.Courses)
            {
                var subject = await _subjectDal.GetAsync(c.SubjectCode);
                subjectList.Add(subject);
            }
            var frontPlan = FrontPlan.Convert(plan, subjectList);
            return frontPlan;
        }

        public async Task RemovePlan(string email, int number)
        {
            await _userDal.RemovePlan(email, number);
        }


        public async Task UpdatePlan(string email, int number, PlanWithOutNumber plan)
        {
            var newPlan = new PlanModel();
            foreach (var c in plan.Courses)
            {
                newPlan.Courses.Add(CourseHeaderModel.From(c));
            }
            await _userDal.UpdatePlan(email, number, newPlan);
        }
    }
}
