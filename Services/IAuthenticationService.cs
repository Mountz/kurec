﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KUREC.Services
{
    interface IAuthenticationService
    {
        public bool Login();
        public bool Logout();
    }
}
