﻿using System.Threading.Tasks;
using KUREC.Models;

namespace KUREC.Services
{
    public interface ITodoAsyncService
    {
        Task<bool> AddAsync(TodoModel model);
        Task<TodoModel[]> GetAllAsync();
        Task<TodoModel> GetLatestAsync();
    }

    public interface ITodoSyncService
    {
        bool Add(TodoModel model);
        TodoModel[] GetAll();
        TodoModel GetLatest();
    }

    public interface ITodoService : ITodoSyncService, ITodoAsyncService
    {

    }
}
