﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KUREC.Models;

namespace KUREC.Services
{
    public class TodoMemoryService : ITodoService
    {
        private List<TodoModel> todoList = new List<TodoModel>(); 
        public bool Add(TodoModel model)
        {
            try
            {
                todoList.Add(model);
            }
            catch(OutOfMemoryException) 
            {
                return false;
            }
            return true;
        }

        public Task<bool> AddAsync(TodoModel model)
        {
            return Task.Run(() => Add(model));
        }

        public TodoModel[] GetAll()
        {
            return todoList.ToArray();
        }

        public Task<TodoModel[]> GetAllAsync()
        {
            return Task.Run(() => GetAll());
        }

        public TodoModel GetLatest()
        {
            return todoList.Last();
        }

        public Task<TodoModel> GetLatestAsync()
        {
            return Task.Run(() => GetLatest());
        }
    }
}
