﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KUREC.Models;
using KUREC.DAL;
using KUREC.Helper;
using KUREC.DTO;
using MongoDB.Driver;

namespace KUREC.Services
{
    public class SubjectService : ISubjectService
    {
        private readonly SubjectDal _subjectDal;

        public SubjectService(SubjectDal subjectDal)
        {
            _subjectDal = subjectDal;
        }

        public async Task<SubjectModel> CreateAsync(SubjectModel model)
        {
            return await _subjectDal.CreateAsync(model);
        }

        public async Task<List<SubjectModel>> GetAllAsync()
        {
            return await _subjectDal.GetAllAsync();
        }

        public async Task<List<SubjectModel>> GetByDateTime(List<DateTimeSpanModel> dateTimeList)
        {
            List<SubjectModel> result = new List<SubjectModel>();
            List<SubjectModel> list = await _subjectDal.GetAllAsync();
            for(int i = list.Count - 1; i >= 0; i--)
            {
                var s = list[i];
                for(int j = s.Courses.Count - 1; j >= 0; j--)
                {
                    var c = s.Courses[j];
                    bool isInTime = false;
                    foreach(var dt in c.DateTime)
                    {
                        foreach (var dateTime in dateTimeList)
                        {
                            if (dt.Day == dateTime.Day &&
                                DateTimeHelper.TimeStringToTimeInSecond(dt.StartTime) >=
                                DateTimeHelper.TimeStringToTimeInSecond(dateTime.StartTime) &&
                                DateTimeHelper.TimeStringToTimeInSecond(dt.EndTime) <=
                                DateTimeHelper.TimeStringToTimeInSecond(dateTime.EndTime)
                                )
                            {
                                isInTime = true;
                            }
                        }
                    }
                    if (!isInTime)
                    {
                        s.Courses.RemoveAt(j);
                    }
                }
                if (s.Courses.Count == 0)
                {
                    list.RemoveAt(i);
                }
            }
            result.AddRange(list);
            return result;
        }

        public async 
            Task<SubjectModel> GetAsync(string code)
        {
            return await _subjectDal.GetAsync(code);
        }

        public Task<List<SubjectModel>> FindByKeyword(string query)
        {
            return _subjectDal.FindByKeyword(query);
        }

        public Task<List<SubjectModel>> FindByCode(string query)
        {
            return _subjectDal.FindByCode(query);
        }

        public Task<List<SubjectModel>> FindByCategory(CategoryFilter filter)
        {
            return _subjectDal.FindByCategory(new CategoryList(filter));
        }
    }
}
